<?php

require_once './vendor/autoload.php';

$name = "Rechnung1.html";
$handler = PhpConsole\Handler::getInstance();
$handler->start();
const URL  = "https://PhantomJsCloud.com/api/browser/v2/";
const API_KEY = "ak-9asja-qn2ph-5g5ce-sf9d7-taczj";

const TEMPLATE_DIR = __DIR__ . "/templates/";
const DATA_DIR = __DIR__ . "/data/";
const INVOICE_DIR = __DIR__ . "/invoices/";


//$handler->debug($_SERVER);
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        handleGet($_GET);
        break;
    case 'POST':
        $request = file_get_contents("php://input");
        $data = json_decode($request, true);
        handleUpdate($data);
        break;
    default:
        exit;
}

function handleGet($req) {

    switch ($req["action"]) {
        case "getDataList": 
            handleDataList();
            break;
        case "createInvoice":
            handleGenerate($req);
            break;
        case "getTemplate": 
            getDataTemplate($req);
            break;
        default:
            handleInvalidRequest();
            break;
    }
};

function handleDataList() {
    $files = preg_grep('/^([^.])/', scandir("data"));
    $presets = [];
    foreach($files as $val) {
        $name = basename($val, ".json");
        array_push($presets, ["name" => $name]);
    }

    global $handler;
    $handler->debug($presets);

    $json = json_encode($presets);
    echo $json;
    exit;
}

function getDataTemplate($req) {
    if (empty($req["template"])) {
        handleInvalidRequest();
    }
    $fileName = $req["template"];
    $fileName = convertFileName($fileName);

    if(file_exists($fileName)) {
        $file = file_get_contents($fileName);
    } else {
        $file = file_get_contents(convertFileName("default"));
    }
    

    echo $file;
    exit;
};

function convertFileName($str) {
    $str = preg_replace('/\s+/', '', $str);
    return DATA_DIR . strtolower($str) . ".json";
}


function handleGenerate($req)
{
    if (empty($req['formData'])) {
        handleInvalidRequest();
    }
    $today = date("d.m.Y");
    $toPay = Date("d.m.Y", strtotime("+10 days"));
    $reqData = $req["formData"];
    $reqData = str_replace("{{toPay}}", $toPay, $reqData);
    $data = json_decode($reqData, true);
    $loader = new Twig_Loader_Filesystem('./templates');
    $twig = new Twig_Environment($loader);
    $data["today"] = $today;
    $data["toPay"] = $toPay;
    $html = $twig->render("template1_print.html", $data);

    //$client = new \GuzzleHttp\Client();
    //$res = $client->request('POST', URL . API_KEY, [
    //    "headers" => [
    //        "Content-Type" => "application/json"
    //    ],
    //    "json" => [
    //        "url" => $html,
    //        "renderType" => "pdf"
    //    ]
    //]);

    //$resBody = $res->getBody();
    //file_put_contents("test.pdf", $resBody);
    $fileName = "Rechnung" . $data["number"] . ".html";
    file_put_contents( INVOICE_DIR . $fileName, $html);

    echo '/invoices/' . $fileName;

    //$html = $twig->render("template1_print.html", $data);


    exit;
};

function handleUpdate($req)
{
    if (!array_key_exists("action", $req) || $req["action"] != "update") {
      handleInvalidRequest(); 
    };
    $formData = $req["formData"];
    $template = $req["template"];
    $fileName = convertFileName($template);
    if ($formData) {
        $json = json_encode($formData);
        file_put_contents($fileName, $json);
    }
    exit;
}

function handleInvalidRequest() {
        http_response_code(403);
        echo "Invalid Request";
        exit;
}





//$mpdf = new \Mpdf\Mpdf(['orientation' => "P"]);
//$mpdf->WriteHTML($html);
//$mpdf->Output();
file_put_contents($name, $html);

//echo "Your file has been saved as ${name}";
echo $html;
