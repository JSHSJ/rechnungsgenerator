# Invoice Generator

I didn't like editing word documents or PDF documents so I chose to build this little program myself.
It's basically a vue frontend with lots of forms which post to a PHP script. This fills a template and returns an HTML file, that you can save as PDF yourself.

### Usage

If you choose to use this yourself, you need run have composer and run a `composer install`.
Then you need to host the directory (such as `php -S localhost:8000`).

There's only one template for invoices right now, which is the one I'm using. You can freely customize it yourself. This one and the php script use the Twig template language.